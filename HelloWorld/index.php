<?php

//echo "Hello World";

//$variableString = "Hello World ";
//$variableInt = 123;
//$variableInt = 100 + $variableInt;
//// équivalent à
//$variableInt += 100;
//
//$variable = $variableString . $variableInt;



//$var = '2 fingers';
//
//const UNE_AUTRE = 'azerty';
//
//define('CONSTANTE', 5);
//
//var_dump();


$var1 = 0;

$var2 = '0';

if ($var2 === false) {
    echo 'oui';
}else {
    echo 'non';
}

//die();

var_dump($var1, $var2);

var_dump(define('CONSTANTE', 'php version') .phpversion());

$var3[] = 'tableau 1';
$var3[] = 'tableau 2';
$var3[] = 'tableau 3';
$var3[50] = 'tableau 4';
$var3[] = 'tableau 5';
$var3[] = 'tableau 6';

$var3[] = 1 . 'un' . 1;

var_dump($var3);

echo '<p>' . date("d-m-Y-H-i-s") . '</p>';


function maFunction(int $nb = 4, int $v = 3) : bool {
    return $nb * $v;
};

function maF(int $v = 3, float $nb = 5.3) : int {
    return $nb * $v;
};

echo maFunction(10000, 5234);

echo '<br>';

echo maF();

die();
?>



//include '/include.php/';
//echo $var5;

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Ceci est un titre</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda cumque ducimus inventore laboriosam numquam,
    odio quam sint soluta unde. Autem cupiditate dolorem minima nemo obcaecati quas sapiente vitae voluptatibus?</p>

<h1><?php echo $variable; ?></h1>
<h1><?= $variable . 123 ?></h1>

<p><?= $varPHP ?></p>
</body>
</html>
