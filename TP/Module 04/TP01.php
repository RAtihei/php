<?php

if (!empty($_POST)) {
    $name = $_POST['nom'];
    $first_name = $_POST['prenom'];
    $adresse = $_POST['adresse'];
    $cp = $_POST['code_postal'];
    $ville = $_POST['ville'];

    foreach ($_POST as $variableDeLaRequete){
        if (empty($variableDeLaRequete)){
            $error = 'Veuillez remplir les champs vides' ;
        }
        if (empty($name)){
            $error1 = 'Veuillez saisir un nom' ;
        }
        if (empty($first_name)){
            $error2 = 'Veuillez saisir un prenom' ;
        }
        if (empty($adresse)){
            $error3 = 'Veuillez saisir une adresse' ;
        }
        if (empty($cp)){
            $error4 = 'Veuillez saisir un code postal' ;
        }
        if (empty($ville)){
            $error5 = 'Veuillez saisir le nom du ville' ;
        }
    }
}
//var_dump($_POST);

?>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adresse client</title>
    <style>
        div {margin: 0.5em}
        .font{font-family: "Arial Black"; font-size: large}
        .error{color: darkred}
    </style>
</head>
<body>
<h1>Page</h1>
<form action="TP01.php" method="post">
    <fieldset>
        <legend>Formulaire adresse</legend><?php echo(isset($error) ? '<span class="font error">'  .$error .'</span>' : ''); ?>
        <div><label>Nom
                <input type="text" name="nom" value="<?= $name ?? '' ?>"></label><?php echo(isset($error1) ? '<span class="error"> '. $error1 . '</span>' : ''); ?>
        </div>
        <div><label>Prénom
                <input type="text" name="prenom" value="<?= $first_name ?? '' ?>"</label><?php echo(isset($error2) ? '<span class="error"> '. $error2 . '</span>' : ''); ?>
        </div>
        <div>
            <label>Adresse
                <input type="text" name="adresse" value="<?= $adresse ?? '' ?>"></label><?php echo(isset($error3) ? '<span class="error"> '. $error3 . '</span>' : ''); ?>
        </div>
        <div>
            <label>CP
                <input type="number" name="code_postal" value="<?= $cp ?? '' ?>"></label><?php echo(isset($error4) ? '<span class="error"> '. $error4 . '</span>' : ''); ?>
        </div>
        <div>
            <label>Ville
                <input type="text" name="ville" value="<?= $ville ?? '' ?>"></label><?php echo(isset($error5) ? '<span class="error"> '. $error5 . '</span>' : ''); ?>
        </div>
        <button type="submit">Envoyer le formulaire</button>
    </fieldset>




</form>
</body>
</html>









