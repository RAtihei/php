<?php

////////////////////////
// Exercice 1   <-----//
echo '<h2>Exercice 1</h2>';

$gerard[] = "gérard";
$gerard[] = "paris";
$gerard[] = 67;

$aurelie[] = "aurélie";
$aurelie[] = "nantes";
$aurelie[] = 31;

$personne["DUPONT"] = $gerard;
$personne["BABIN"] = $aurelie;

var_dump($personne);

////////////////////////
// Exercice 2   <-----//
echo '<h2>Exercice 2</h2>';

$personne_1 = [
    "DUPONT" =>
        ["prénom" => "gérard", "ville" => "paris", "âge" => 67]
];
var_dump($personne_1);

$personne_1 = [
    "BABIN" =>
        ["prénom" => "aurélie", "ville" => "nantes", "âge" => 31]
];
var_dump($personne_1);

////////////////////////
// Exercice 3   <-----//
echo '<h2>Exercice 3</h2>';

echo '<ul>';
foreach ($personne as $nom => $valeurs) {
    echo '<li>Element ' . $nom . '<ul>';
    for ($i = 0; $i < count($valeurs); $i++) {
        echo '<li>élément' . $i . ' : ' . $valeurs[$i] . '</li>';
    }
    echo '</ul></li>';
}
echo '</ul>';

////////////////////////
// Exercice 4   <-----//
echo '<h2>Exercice 4</h2>';

$mail = ['zhiyuancai@google.com', 'louischiu@google.com', 'berryhill@google.com',
    'stevenvanni@google.com', 'beder@google.com', 'marcvincent@google.com',
    'marktomlinson@google.com', 'filiprad@google.com', 'ameurhosni@google.com'];
$domaines = [];
foreach ($mail as $m) {
    $d = explode('@', $m)[1];
//    if (!empty($domaines) && array_key_exists($d, $domaines)){
    if (isset($domaines[$d])) {
        $domaines[$d]++;
    } else {
        $domaines[$d] = 1;
    }
}
var_dump($domaines);

////////////////////////
// Exercice 5   <-----//
echo '<h2>Exercice 5</h2>';

for ($i = 1; $i <= 63; $i++) {
    $t1[] = $i;
}
$t2[] = 0;
foreach ($t1 as $val) {
    $t2[] = $val / 10;
}

foreach ($t2 as $reel) {
    $t3[(string)$reel] = sin($reel);
}
?>
<table style="border-collapse: collapse">
    <tr>
        <th style="border: 1px solid black">x</th>
        <th style="border: 1px solid black">sin (x)</th>
    </tr>
    <?php
    foreach ($t3 as $x => $sinx) {
        echo '<tr><td style="border: 1px solid black">' . $x . '</td><td style="border: 1px solid black">' . $sinx . '</td></tr>';
}
    ?>
</table>