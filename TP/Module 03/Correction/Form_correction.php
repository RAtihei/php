<?php

class Form_correction {
    protected string $formulaire;
    public function __construct(string $name) {
    $this->formulaire = '<form action="" method="POST" name="'.$name.'">';
}

    public function setInputText(string $name, string $label): void
    {
        $this->formulaire .= '<label for="' . $name .'">' . $label . '</label>';
        $this->formulaire .= '<input id="' . $name . '" type="text" name="' . $name . '" value=""/>';
        $this->formulaire .= '<br/>';
    }

    public function setSubmit(string $name): void
    {
        $this->formulaire .= '<button type="submit" name="'.$name.'">OK</button>';
    }

    public function getForm(): string
    {
        $this->formulaire = '<form> <fieldset>' . $this->formulaire . '</fieldset> </form>';
        return $this->formulaire;
    }
}