<?php
require_once 'Form_correction.php';

class Form2_correction extends Form_correction
{
    public function setRadioInput(string $name, string $label, string $optionName ): void
    {
        $this->formulaire .= '<input id="'.$optionName.'" name="'.$name.'" type="radio" value="'.$optionName.'">';
        $this->formulaire .= '<label for="'.$name.'">'.$label.'</label>';
        $this->formulaire .= '<br/>';
    }

    public function setCheckboxInput(string $name, string $label): void
    {
        $this->formulaire .= '<input id="'.$name.'" name="'.$name.'" type="checkbox">';
        $this->formulaire .= '<label for="'.$name.'">'.$label.'</label>';
        $this->formulaire .= '<br/>';
    }
}