<?php
require_once 'Form2_correction.php';

$message = "";

if (!empty($_POST)) {

    if (!$age = filter_input(INPUT_POST,'age', FILTER_SANITIZE_NUMBER_INT)) {
        $message .= "L'age n'est pas valide<br/>";
    }

    if (!$message) {
        echo htmlspecialchars($_POST['nom']);
        die('OK ! c`est bon');
    } else {
        echo $message;
    }
}

$form = new Form2_correction('formulaire_contact');
$form->setInputText('nom', 'Nom de Famille');
$form->setInputText('prenom', 'Prénom');
$form->setInputText('age', 'Age');
$form->setRadioInput('sexe', 'Féminin', 'F');
$form->setRadioInput('sexe', 'Masculin', 'M');
$form->setRadioInput('résident', 'Résident', 'O');
$form->setRadioInput('résident', 'Non résident', 'N');
$form->setCheckboxInput('accept', "J'accepte les Conditions");
$form->setSubmit('submit');

echo $form->getForm();

