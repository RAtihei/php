<?php

class  VilleAvecConstructeur
{
    protected string $nom;
    private string $departement;

    /**
     * @param string $nom
     * @param string $departement
     */
    public function __construct(string $nom, string $departement)
    {
        $this->nom = $nom;
        $this->departement = $departement;
    }

    public function __toString() : string
    {
        return sprintf('La ville %s est dans le département %s', $this->nom, $this->departement);
    }
}