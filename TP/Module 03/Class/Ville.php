<?php

class Ville {
    private string $nom;
    private string $departement;

    public function getNom() : string {
        return $this->nom;
    }
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getDepartement(){
        return $this->departement;
    }
    public function setDepartement(string $departement): void
    {
        $this->departement = $departement;
    }

    public function __toString() : string
    {
        return sprintf('La ville %s est dans le département %s', $this->nom, $this->departement);
    }
}

?>