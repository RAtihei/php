<?php

class VilleAvecRegion extends VilleAvecConstructeur {

    private string $region;
    private static string $villeAuNomLePlusLong = "";

    /**
     * @param string $region
     */
    public function __construct(string $nom, string $departement, string $region = null)
    {
        parent::__construct($nom, $departement);
        $this->region = $region;

        if (strlen($this->nom) > strlen(static::$villeAuNomLePlusLong)){
            static::$villeAuNomLePlusLong = $this->nom;
        }
    }

    public function getRegion(): string
    {
        return $this->region;
    }
    public function setRegion(string $region): void
    {
        $this->region = $region;
    }

    public function __toString() : string
    {
        return parent::__toString() . sprintf(' et dans la région %s', $this->region);
    }

    /**
     * @return string
     */
    public static function getVilleAuNomLePlusLong(): string
    {
        return static::$villeAuNomLePlusLong;
    }
}