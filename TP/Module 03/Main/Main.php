
<?php

////////////////////////////////////
echo '<h2>Module 03 - TP01</h2>';///
////////////////////////////////////
require_once '../Class/Ville.php';
require_once '../Class/VilleAvecConstructeur.php';
require_once '../Class/VilleAvecRegion.php';
require_once '../Main/Main.php';


// Ex 1
$ville = new Ville();
$ville->setNom('Dijon');
$ville->setDepartement('Cote d\'or');

echo '<h3>' . $ville . '</h3>';

// Ex 2
$ville2 = new VilleAvecConstructeur('Nantes', '44100');

echo '<h3>' . $ville2 . '</h3>';

// Ex 3
$ville3 = new VilleAvecRegion('Quimper','Finistère', 'Bretagne');

echo '<h3>' . $ville3 . '</h3>';

// Ex 4
$ville4 = new VilleAvecRegion('Kirschnaumen', 'Moselle', 'Lorraine');
$ville5 = new VilleAvecRegion('Kedange-sur-Canner', 'Moselle', 'Lorraine');
$ville6 = new VilleAvecRegion('Metzeresche', 'Moselle', 'Lorraine');

echo '<h3>La ville avec le nom le plus long est : ' . VilleAvecRegion::getVilleAuNomLePlusLong() . '</h3>';

// Ex 5
//$coucou = new Form();
//
//echo $coucou->creationFormulaire();
//



/*<form action="/action_page.php">
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname" value="John"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname" value="Doe"><br><br>
  <input type="submit" value="Submit">
</form> */



die();




?>
<!--<table style="border-collapse: collapse">-->
<!--    <tr>-->
<!--        <th style="border: 1px solid black">x</th>-->
<!--        <th style="border: 1px solid black">sin (x)</th>-->
<!--    </tr>-->
<!--    --><?php
//    foreach ($t3 as $x => $sinx) {
//        echo '<tr><td style="border: 1px solid black">' . $x . '</td><td style="border: 1px solid black">' . $sinx . '</td></tr>';
//}
//    ?>
<!--</table>-->