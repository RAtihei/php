<?php

try {
    $dsn = 'mysql:host=localhost;dbname=commerce';

    $options = [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"];

    $pdo = new PDO($dsn, 'root', '', $options);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    echo 'Connexion réussie';
}
catch (PDOException $e){
    $msg = 'ERREUR PDO dans' . $e->getFile() . ' : ' . $e->getLine() . ' : ' . $e->getMessage();
    die($msg);
}

/////////////////////
//  Exemple 1  //////
//$query = 'SELECT * FROM articles where identifiant = 1';
//$arr = $pdo->query($query)->fetch();
//var_dump($arr);

/////////////////////
//  Exemple 2  //////
//$query = 'SELECT * FROM articles';
//$arr = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
//var_dump($arr);

/////////////////////
//  Exemple 3  //////
//$id = 3;
//$query = 'SELECT * FROM articles where identifiant = ' . $id;
//$arr = $pdo->query($query)->fetch(PDO::FETCH_ASSOC);
//var_dump($arr);

/////////////////////
//  Exemple 4  //////
//$id = "3, DROP TABLE articles";
//$id = "1";
//$query = 'SELECT * FROM articles where identifiant = :identifiant';
//$prep = $pdo->prepare($query);
//$prep->bindValue(':identifiant', $id);
//$prep->execute();
//$arr = $prep->fetchAll(PDO::FETCH_ASSOC);
//var_dump($arr);

/////////////////////
//  Exemple 5  //////

//------> INSERT INTO
//$query = "INSERT INTO articles(libelle, prix) VALUES (:article, :prix)";
//$prep = $pdo->prepare($query);
//$prep->bindValue(':article', 'tomates');
//$prep->bindValue(':prix', 45.23);
//$prep->execute();
//echo " Nombre d'articles insérées : " . $prep->rowCount();

//------> UPDATE
//$query = "UPDATE articles SET prix= :prix WHERE identifiant= :id";
//$prep = $pdo->prepare($query);
//$prep->bindValue(':prix', 52.36);
//$prep->bindValue(':id', 5);
//$prep->execute();
//echo " Nombre d'articles modifiés : " . $prep->rowCount();

$query = "INSERT INTO articles(libelle, prix) VALUES (:article, :prix)";
$prep = $pdo->prepare($query);
$prep->bindParam(':article', $libelle);
$prep->bindParam(':prix', $prix);

$libelle = 'Poireaux';
$prix = 31.87;
$prep->execute();

$libelle = 'Navets';
$prix = 6.87;
$prep->execute();

$query = 'SELECT * FROM articles';
$prep = $pdo->prepare($query);
$prep->execute();
$arr = $prep->fetchAll(PDO::FETCH_ASSOC);
var_dump($arr);

?>

<?php
/*try {
// chaine de connexion à la base de données
    $dsn = 'mysql:host=localhost;dbname=legumes';

// option de connexion : encodage UTF8 pour MySQL
    $options = [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"];

// création d'une instance de connexion à la base de données et ouverture de
// Connexion
    $pdo = new PDO($dsn, 'userCodePHP', 'YMRwhDxHQunQ9h7M', $options);

// choix de la méthode d'information en cas d'erreur : levée d'exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    echo 'connexion effectuée avec le driver ' . $pdo->getAttribute(PDO::ATTR_DRIVER_NAME) . '<br>';

} catch (PDOException $e) {
    $msg = 'ERREUR PDO dans ' . $e->getFile() . ' : ' . $e->getLine() . ' : ' . $e->getMessage();
    die($msg);
}*/

