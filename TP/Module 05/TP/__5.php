<?php

////////////////////////
// Exercice 1   <-----//
echo '<h2>Exercice 1</h2>';

$C = 'bOnJoUr, jE sUiS UnE pHrAsE pOuR tEsTeR';
echo ucwords(strtolower($C)) . '<br>';

$phrase = ucwords('bonjour je suis une phrase pour tester');
var_dump($phrase);

////////////////////////
// Exercice 2   <-----//
echo '<h2>Exercice 2</h2>';

$personne = [
    ['Atihei', 'REY'],
    ['Gérard', 'ZATA'],
    ['Aurélie', 'ZOLO'],
];

echo '<pre style="font-family: courier">';
foreach ($personne as $p){
    printf("%-20s %-10s<br>", $p[0], $p[1]);
}
echo '</pre>';

////////////////////////
// Exercice 3   <-----//
echo '<h2>Exercice 3</h2>';

$listeCours = [
    '5 - Projet n°1',
    '1 - Algo',
    '3 - HTML/CSS',
    '4 - JavaScript',
    '2 - Java'
];

natsort($listeCours);

echo '<ul>';
foreach ($listeCours as $cours) {
    echo '<li>'.$cours.'</li>';
}
echo '</ul>';

////////////////////////
// Exercice 4   <-----//
echo '<h2>Exercice 4</h2>';

$maintenant = time();
$naissance = mktime(13, 55, 0, 6, 13, 1991);
echo number_format($maintenant - $naissance, 0, '', ' ')." secondes vécues";

////////////////////////
// Exercice 5   <-----//
echo '<h2>Exercice 5</h2>';

switch (date('N', mktime(0,0,0,12,25,2017))){
    case 1:
        echo 'Lundi';
        break;
    case 2:
        echo 'Mardi';
        break;
    case 3:
        echo 'Mercredi';
        break;
    case 4:
        echo 'Jeudi';
        break;
    case 5:
        echo 'Vendredi';
        break;
    case 6:
        echo 'Samedi';
        break;
    case 7:
        echo 'Dimanche';
        break;
}

////////////////////////
// Exercice 6   <-----//
echo '<h2>Exercice 6</h2>';

for ($i = 2018; $i < 2037; $i++) {
    echo "<li>Année " .$i . ": ";
    switch(date('N', mktime(0,0,0,5,1,$i))){
        case 1:
        case 5:
            echo 'WE prolongé';
            break;
        case 2:
        case 4:
            echo 'Pont à négocier';
            break;
        case 6:
        case 7:
            echo 'Désolé';
            break;
        default:
            echo 'C\'est donc un mercredi, c\'est bien aussi';
    }
    echo "</li>";
}

////////////////////////
// Exercice 7   <-----//
echo '<h2>Exercice 7</h2>';

for ($i = 2018; $i < 2037; $i++) {
    $paques = easter_date($i);
    $ascension = $paques * 39 * 24 * 60 * 60;
    echo "en l'année $i, l'ascension sera le ".date('l m/d', $ascension).'<br/>';
}
echo (new DateTime('+ 4 days')) -> format('d/m/Y');

?>