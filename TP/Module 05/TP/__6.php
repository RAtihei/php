<?php

////////////////////////
// Exercice 1   <-----//
echo '<h2>Exercice 1</h2>';

$v1 = true;
$v2 = 42;
$v3 = 12.34;
$v4 = 'Hello';

var_dump($v1);
var_dump($v2);
var_dump($v3);
var_dump($v4);

////////////////////////
// Exercice 2   <-----//
echo '<h2>Exercice 2</h2>';

$x = "PostgreSQL";
$y = "MySQL";
$z = &$x;
$x = "PHP 5";
$y = &$x;

var_dump($z . ', ' . $x . ', ' . $y);

////////////////////////
// Exercice 3   <-----//
echo '<h2>Exercice 3</h2>';
function alpha($a = "0",
               $b = "TRUE",
               $c = FALSE,
): bool
{
    $d = ($a or $b);
    $e = ($a and $c);
    $f = ($a xor $b);

    return $d . $e . $f;
}
var_dump(alpha());

////////////////////////
// Exercice 4   <-----//
echo '<h2>Exercice 4</h2>';

$X = "PHP7";
var_dump($X);
$A = &$X;
var_dump($A);
$Y = " 7 eme version de PHP";
var_dump($Y);

// WARNING  <-----//
//$Z = $Y * 10;
//var_dump($Z);
//$X = $Y * $Y;
//var_dump($X);

?>