<?php

////////////////////////
// Exercice 1   <-----//
echo '<h2>Exercice 1</h2>';

function nb(int $nb) : int{
    $nbTentative = 0;
    while (rand(0,999) !== $nb){
        $nbTentative++;
    }
    return $nbTentative;
}

$nb_a_trouver = 55;

echo nb($nb_a_trouver). ' pour tirer le nombre ' . $nb_a_trouver;

////////////////////////
// Exercice 2   <-----//
echo '<h2>Exercice 2</h2>';

function ordonner(int &$a, int &$b){
    if ($a < $b){
        $t = $a;
        $a = $b;
        $b = $t;
    }
}

$a = rand(1,10);
$b = rand(1,10);

echo 'Au départ $a vaut : ' . $a . ' et $b vaut : ' . $b . '<br/>';

ordonner($a, $b);

echo 'Après appel à la fonction $a vaut : ' . $a . ' et $b vaut : ' . $b . '. $a est supérieur ou égal à $b';

////////////////////////
// Exercice 3   <-----//
echo '<h2>Exercice 3</h2>';

$a = rand(1, 10000);
$b = rand(1, 10000);

function f(int $a, int $b): int{

//    ordonner($a, $b);
    do{
        $r = $a%$b;
        $a = $b;
        $b = $r;
    }while ($r !== 0);
    return $a;
}

echo 'Le plus grand dénominateur commun de $a et $b est '. f($a, $b). '<br/>';

////////////////////////
// Exercice 4   <-----//
echo '<h2>Exercice 4</h2>';



////////////////////////
// Exercice 5   <-----//
echo '<h2>Exercice 5</h2>';



////////////////////////
// Exercice 6   <-----//
echo '<h2>Exercice 6</h2>';



////////////////////////
// Exercice 7   <-----//
echo '<h2>Exercice 7</h2>';


?>